#include <Wire.h>
#include <SoftwareSerial.h>

#define BT_SERIAL_TX 0
#define BT_SERIAL_RX 1

#define D6T_ID 0X0A
#define D6T_CMD 0X4C
SoftwareSerial BluetoothSerial(BT_SERIAL_TX, BT_SERIAL_RX); 


int ReadBuffer[35]; 
float ptat; 
float tdata[16]; 



void setup()
{

Wire.begin(); 
BluetoothSerial.begin(9600); 
delay(500); 

  
  
}
void loop()
{
int i; 
//requesting data from sensor
Wire.beginTransmission(D6T_ID); 
Wire.write(D6T_CMD); 
Wire.endTransmission(); 

//recieving data
Wire.requestFrom(D6T_ID, 35); 

for(i = 0; i<35; i++){
  ReadBuffer[i] = Wire.read(); 
}

ptat = (ReadBuffer[0]+(ReadBuffer[1]*256))*0.1; 

for(i = 0; i<16; i++){
  tdata[i] = (ReadBuffer[(i*2+2)]+(ReadBuffer[(i*2+3)]*256))*0.1; 
  
}

float tempF; 
if( ((tdata[0]*9.0/5.0)+32.0) > 0){
  for(i = 0; i<16; i++){
    tempF = (tdata[i]*9.0/5.0)+32.0; 

    BluetoothSerial.print(tempF); 
    BluetoothSerial.print(','); 
    
  }
  BluetoothSerial.print((ptat*9.0/5.0)+32.0); 
  BluetoothSerial.print(','); 
  BluetoothSerial.println(); 
  
}

  
  
}

