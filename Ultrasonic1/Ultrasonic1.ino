
const int trigPin1 = 7;
const int echoPin1 = 6;

long duration1;
int distance1; 

//int redled = 13; 
 

void setup() {
//  pinMode(redled, OUTPUT); 
   
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  Serial.begin(9600);

}

void loop() {
  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);

  pinMode(echoPin1, INPUT);
  duration1 = pulseIn(echoPin1, HIGH);
  
  distance1 = duration1 * 0.034/2; 

  Serial.print ("Distance1: ");
  Serial.println(distance1);

 /* if(distance > 150){
    digitalWrite(redled, HIGH); 
    delay(1000); 
    digitalWrite(redled, LOW); 
    delay(1000); 
    }
    */

  
  }
