#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>

//RX pin = 2, TX pin = 3
SoftwareSerial mySerial(3, 2); 

Adafruit_GPS GPS(&mySerial); 


//turns off raw GPS sentences
#define GPSECHO false 
 

boolean usingInterrupt = false; 
void useInterrupt(boolean); 

void setup() {
  //sets baud rate to 115200 
  Serial.begin(115200); 

  //default baud rate
  GPS.begin(9600);

  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY); 
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); 
  GPS.sendCommand(PGCMD_ANTENNA); 

  useInterrupt(true); 
  delay(2000); 
  mySerial.println(PMTK_Q_RELEASE); 
  
}

//Interrupt is called once every two milliseconds, looks for new GPS data, stores it 
SIGNAL(TIMER0_COMPA_vect){
  char x = GPS.read(); 
  #ifdef UDR0
  if(GPSECHO)
    if(x) UDR0 = x; 

 #endif
}

void useInterrupt(boolean v){
  if(v){
    OCR0A = 0xAF; 
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true; 
  } else{
    TIMSK0 &= ~_BV(OCIE0A); 
    usingInterrupt = false; 
  }
}

uint32_t timer = millis(); 

void loop() {

  if(! usingInterrupt){
    char x = GPS.read(); 
    if(GPSECHO)
      if(x) Serial.print(x); 
  }

  if(GPS.newNMEAreceived()){
    Serial.println(GPS.lastNMEA()); 

      if(!GPS.parse(GPS.lastNMEA()))
      return; 
  }

  if(timer > millis()) timer = millis(); 

  if(millis() - timer > 2000){
    timer = millis(); 

    Serial.print("\nTime: ");
    Serial.print(GPS.hour, DEC); Serial.print(':');
    Serial.print(GPS.minute, DEC); Serial.print(':');
    Serial.print(GPS.seconds, DEC); Serial.print('.');
    Serial.println(GPS.milliseconds);
    
    Serial.print("Date: ");
    Serial.print(GPS.day, DEC); Serial.print('/');
    Serial.print(GPS.month, DEC); Serial.print("/20");
    Serial.println(GPS.year, DEC);

    GPS.fix;
    GPS.fixquality;

    if(GPS.fix){
      
      Serial.print("Location in degrees: ");
      Serial.print(GPS.latitudeDegrees, 4);
      Serial.print(", "); 
      Serial.println(GPS.longitudeDegrees, 4);

      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      
    }
    
  }
}

















