import processing.serial.*; 

//String portName = "/dev/cu.HC-06-DevB-2"; 
String portName = "/dev/cu.usbmodem1421"; 
Serial BT; 
int BaudRate = 9600; 
String inString; 

float[] tdata = new float[17]; 

String buff; 
PFont font; 
float colorGreen;
int lbTemp = 60; 
int hbTemp = 90; 

void setup(){
  size(640, 640); 
  BT = new Serial(this, portName, BaudRate); 
  BT.bufferUntil('\n'); 

  font = loadFont("STIXNonUnicode-Regular-40.vlw"); 
  textFont(font, 40); 
  
}

void draw(){
  background(0,0,0); 
  for(int i = 0; i< 16; i++){
    colorGreen = map(tdata[i], lbTemp, hbTemp, 255, 0); 
    fill(255, colorGreen, 0); 

    rect((i % 4)* 160, floor(i/4)*160, 160, 160); 
    if(tdata[i]<5) {fill(255);}else{ fill(0); }

    textAlign(CENTER, CENTER); 
    textSize(40); 
    text(str(tdata[i]), (i % 4)*160+80, floor(i / 4)*160+80); 
    textSize(20); 

    text("Relative Temperature"+str(tdata[16]), 140, 10); 
  }
}

void serialEvent(Serial BT){
  inString = BT.readString(); 
  tdata = float(split(inString, ',')); 
}