#include <SoftwareSerial.h>
#define BT_SERIAL_TX 1
#define BT_SERIAL_RX 0
SoftwareSerial BluetoothSerial(BT_SERIAL_TX, BT_SERIAL_RX); 


const int trigPin1 = 7;
const int echoPin1 = 6;

const int trigPin2 = 9;
const int echoPin2 = 8;

const int trigPin3 = 13;
const int echoPin3 = 12;

const int trigPin4 = 5;
const int echoPin4 = 4;

long duration1;
int distance1; 

long duration2;
int distance2; 

long duration3;
int distance3;

long duration4;
int distance4;
 

void setup() {

 
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  BluetoothSerial.begin(9600);

  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  BluetoothSerial.begin(9600);

  pinMode(trigPin3, OUTPUT);
  pinMode(echoPin3, INPUT);
  BluetoothSerial.begin(9600);

  pinMode(trigPin4, OUTPUT);
  pinMode(echoPin4, INPUT);
  BluetoothSerial.begin(9600);



}

void loop() {

  //sensor 1 
  
  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);

  pinMode(echoPin1, INPUT);
  duration1 = pulseIn(echoPin1, HIGH);
  
  distance1 = duration1 * 0.034/2; 

  BluetoothSerial.print ("Distance1: ");
  BluetoothSerial.println(distance1);

  //sensor 2

  digitalWrite(trigPin2, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin2, LOW);

  pinMode(echoPin2, INPUT);
  duration2 = pulseIn(echoPin2, HIGH);
  
  distance2 = duration2 * 0.034/2; 

  BluetoothSerial.print ("Distance2: ");
  BluetoothSerial.println(distance2);

  //sensor 3 
  digitalWrite(trigPin3, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin3, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin3, LOW);

  pinMode(echoPin3, INPUT);
  duration3 = pulseIn(echoPin3, HIGH);
  
  distance3 = duration3 * 0.034/2; 

  BluetoothSerial.print ("Distance3: ");
  BluetoothSerial.println(distance3);


  //sensor 4
  digitalWrite(trigPin4, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin4, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin4, LOW);

  pinMode(echoPin4, INPUT);
  duration4 = pulseIn(echoPin4, HIGH);
  
  distance4 = duration4 * 0.034/2; 

  BluetoothSerial.print ("Distance4: ");
  BluetoothSerial.println(distance4);
  
  
  }
